FROM nginx:latest

COPY . /usr/share/nginx/html

EXPOSE 8009

CMD ["nginx", "-g", "daemon off;"] 
